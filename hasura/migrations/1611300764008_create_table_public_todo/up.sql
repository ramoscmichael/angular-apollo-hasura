CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE TABLE "public"."todo"("id" uuid NOT NULL DEFAULT gen_random_uuid(), "todo" varchar NOT NULL, "completed" integer NOT NULL DEFAULT 0, "create_dt" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") );
