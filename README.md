# Angular using Apollo and Hasura

### Run Postgre and Hasura service using docker-compose

        docker-compose up

### To view hasura console

-   Open http://localhost:8080/console
-   input the secret key >> `imnolongerasecret`

### Build and start project

        npm install
        npm run start

#### Explore and enjoy

Reference: https://hasura.io/docs/1.0/graphql/core/queries/index.html