import { NgModule } from "@angular/core";
import { environment } from "src/environments/environment";
import { GraphCoreModule } from "src/graph/graph.module";
import { TokenService } from "./services/token.service";

@NgModule({
    imports: [
        GraphCoreModule.forRoot({
            WS_GRAPH_URI: environment.wsUrl,
            HTTP_GRAPH_URI: environment.apiUrl,
            TOKEN: TokenService,
        })
    ],
    providers: [
        TokenService,
    ]
})
export class DataModule {
}