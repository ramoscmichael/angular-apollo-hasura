import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { environment } from "src/environments/environment";
import { GraphTokenHandler } from "src/graph/services/graph.options";

@Injectable()
export class TokenService implements GraphTokenHandler {

    subject: BehaviorSubject<any> = new BehaviorSubject({
        headers: {
            Authorization : environment.token
        }
    }); 
    observable = this.subject.asObservable();

    constructor() {}

    token() {
        return this.observable;
    }
}
