import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { TodoRoutingModule } from "./todo-routing.module";
import { TodoComponent } from "./todo.component";

@NgModule({
    declarations: [TodoComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TodoRoutingModule,
    ],

    exports:[RouterModule]
})
export class TodoModule {
}