import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import {  Observable } from 'rxjs';
import { createGraphType, deleteQl, GraphService, insertQl, queryQl, updateQl } from 'src/graph/services/graph.service';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';

const TODO_TYPE = createGraphType('todo', 'id, todo, completed');

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  public todo = new FormControl('');
  public todos;
  constructor(private readonly graph: GraphService) { }

  ngOnInit(): void {
    this.todos = this.graph.subscription(
      queryQl(TODO_TYPE('todos'))
    ).pipe(
      map((r: any) => (r.data.todos || []).sort((a, b) => a.todo.localeCompare(b.todo))),
    )
  }

  addTodo() {
    this.graph.mutation(
      insertQl(TODO_TYPE(), {
        objects: {
          todo: this.todo.value
        }
      }),
    ).toPromise()
      .finally(() => this.todo.setValue(''));

  }

  deleteTodo(todo) {
    this.graph.mutation(
      deleteQl(TODO_TYPE(), {
        id: {
          _eq: todo.id
        }
      })
    ).toPromise()
      .finally(() => this.todo.setValue(''));
  }

  completeTodo(todo) {
    this.graph.mutation(
      updateQl(TODO_TYPE(), {
        set: {
          completed: 1
        },
        where: {
          id: {
            _eq: todo.id
          }
        }
      })
    ).toPromise()
      .finally(() => this.todo.setValue(''));
  }

}
