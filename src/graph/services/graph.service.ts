import { Injectable } from "@angular/core";
import { Apollo, gql } from "apollo-angular";
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";

export interface QueryCondition {
    where?: any;
    offset?: number;
    args?: any;
    distinct_on?: any;
    limit?: number;
    order_by?: any[];
}

export interface GraphType {
    alias: string;
    name: string;
    fields: string;
    derivedName: string;
}

export type GraphTypeCreator = (alias?: string) => GraphType;

export function createGraphType(name: string, fields: string, derivedName?: string): GraphTypeCreator {
    derivedName = derivedName || name;
    return (alias) => ({
        name, fields, derivedName, alias
    });
}

export interface GraphQLStatement {
    gql: string;
    parameters: string[];
    variables: any;
}

export type QueryQLStatementFunction = (i: any) => GraphQLStatement;
export type MutationQLStatementFunction = (i: any) => GraphQLStatement;

export function queryQl(type: GraphType,  condition: QueryCondition = {}): QueryQLStatementFunction  {
    const alias = type.alias ? `${type.alias}:` : '';
    return (i: any) => {
        const argsQl = !!condition.args ?  `args: $args_${i},` : '';
        const gql = `${alias} ${type.derivedName} (${argsQl} where: $where_${i}, offset: $offset_${i}, limit: $limit_${i}, order_by: $order_by_${i}, distinct_on: $distinct_on_${i}) {
            ${type.fields}
        }`;

        const options: any = {
            gql,
            parameters: [
                `$where_${i}: ${type.name}_bool_exp`,
                `$offset_${i}: Int`,
                `$limit_${i}: Int`,
                `$order_by_${i}: [${type.name}_order_by!]`,
                `$distinct_on_${i}: [${type.name}_select_column!]`
            ],
            variables: { 
                [`where_${i}`]: condition.where,
                [`offset_${i}`]: condition.offset,
                [`limit_${i}`]: condition.limit,
                [`order_by_${i}`]: condition.order_by,
                [`distinct_on_${i}`]: condition.distinct_on
            }
        };

        if (condition.args) {
            options.parameters.push(`$args_${i}: ${type.derivedName}_args!`);
            options.variables[`args_${i}`] = condition.args
        }

        return options;
    };
}

export function aggregateQl(type: GraphType, aggregate: string, where?: any): QueryQLStatementFunction {
    const alias = type.alias ? `${type.alias}:` : '';
    return (i) => {
        const gql = `${alias} ${type.name}_aggregate (where: $where_${i}) {
            ${aggregate}
        }`;

        return {
            gql,
            parameters: [
                `$where_${i}: ${type.name}_bool_exp`
            ],
            variables: {
                [`$where_${i}`]: where
            }
        }
    };
}

export function insertQl(type: GraphType, data: { objects: any | any[], on_conflict?: any }): MutationQLStatementFunction {
    const objects = Array.isArray(data.objects) ? data.objects : [data.objects];
    const alias = type.alias ? `${type.alias}:` : '';
    return (i) => {
        const onConflict = !!data.on_conflict ? `on_conflict: $on_conflict_${i}` : '';
        const gql = `
            ${alias} insert_${type.name}(${onConflict} objects: $entities_${i}) {
                returning {
                    ${type.fields}
                }
            }
        `;
        const options: any = {
            gql,
            parameters: [
                `$entities_${i}: [${type.name}_insert_input!]!`
            ],
            variables: {
                [`entities_${i}`]: objects
            }
        };

        if(data.on_conflict) {
            options.parameters.push(`$on_conflict_${i}: ${type.name}_on_conflict`);
            options.variables[`on_conflict_${i}`] = data.on_conflict;
        }

        return options;
    };
}

export function updateQl(type: GraphType, data: { set: any, where?: any }): MutationQLStatementFunction {
    const alias = type.alias ? `${type.alias}:` : '';
    return (i) => {
        const gql = `${alias} update_${type.name}(_set: $entity_${i}, where: $where_${i}) {
            returning {
                ${type.fields}
            }
        }`;

        return {
            gql,
            parameters: [
                `$entity_${i}: ${type.name}_set_input!`,
                `$where_${i}: ${type.name}_bool_exp!`
            ],
            variables: {
                [`entity_${i}`]: data.set,
                [`where_${i}`]: data.where
            }
        };
    }
}

export function deleteQl(type: GraphType, where: any): MutationQLStatementFunction {
    const alias = type.alias ? `${type.alias}:` : '';
    return (i) => {
        const gql = `${alias} delete_${type.name} (where: $where_${i}) {
                        returning {
                            ${type.fields}
                        }
                    }`;

        return {
            gql,
            parameters: [
                `$where_${i}: ${type.name}_bool_exp!`
            ],
            variables: {
                [`where_${i}`]: where
            }
        }
    };
}

export interface PageResult {
    totalRecords: number;
    totalPages: number;
    records: any[];
    page: number;
}

export class Pagination {

    private readonly result: PageResult = {
        totalPages: 0,
        totalRecords: 0,
        records: [],
        page: 1
    };

    private subject = new BehaviorSubject<PageResult>(this.result);

    private readonly graph: GraphService;
    private readonly type: GraphTypeCreator;
    private condition: QueryCondition;

    constructor(graph: GraphService,
                type: GraphTypeCreator,
                condition: QueryCondition) {

        this.type = type;
        this.graph = graph;
        this.condition = JSON.parse(JSON.stringify(condition));
        this.condition.limit = this.condition.limit || 25;
        this.page(1);
    }

    refresh(condition: QueryCondition = {}) {

        this.condition = JSON.parse(JSON.stringify({ ... this.condition, ... condition }));
        this.condition.offset = ((a, b) => ( (c: any) => (!c || c) -1 ) ((a * b) - b) )(this.result.page, this.condition.limit);

        this.graph.query(
            aggregateQl(this.type('page'), `aggregate { count }`, this.condition.where),
            queryQl(this.type('results'), this.condition)
        ).pipe(
            map(
                (res: any) => {
                    this.result.totalRecords = ((res.data.page || {}).aggregate || {}).count || 0;
                    this.result.totalPages = Math.ceil(this.result.totalRecords / this.condition.limit);
                    this.result.records = res.data.results || [];

                    return JSON.parse(JSON.stringify(this.result));
                }
            )
        ).subscribe(page => this.subject.next(page));
    }

    page(n: number) {
        this.result.page = n;
        this.refresh();
    }

    public data() {
        return this.subject.asObservable();
    }
}

@Injectable()
export class GraphService {
    constructor(private apollo: Apollo) {}

    public pagination(type: GraphTypeCreator, condition: QueryCondition) {
        return new Pagination(this, type, condition);
    }

    private statement(statements: QueryQLStatementFunction[] | MutationQLStatementFunction[]) {
        const preparedStatements = statements.map((m, i) => m(i) as GraphQLStatement);
        const types = preparedStatements.reduce((c, e) => [... c, ... e.parameters], []).join(', ');
        const gql = preparedStatements.reduce((c, e) => [... c, e.gql], []).join(' \n ');
        const variables = preparedStatements.reduce((c, e) => ({ ...c, ... e.variables }), {});

        return {
            types,
            gql,
            variables
        }
    }
    
    query(... statements: QueryQLStatementFunction[]) {
        const resp = this.statement(statements);
        return this.apollo.query({
            query: gql`
                query (${resp.types}) {
                    ${resp.gql}
                }
            `,
            variables: resp.variables
        });
    }

    mutation(... statements: MutationQLStatementFunction[]) {
        const resp = this.statement(statements);
        return this.apollo.mutate({
            mutation: gql`
                mutation (${resp.types}) {
                    ${resp.gql}
                }
            `,
            variables: resp.variables
        });
    }

    subscription(statement: QueryQLStatementFunction) {
        const resp = this.statement([statement]);
        return this.apollo.subscribe({
            query: gql`
                subscription (${resp.types}) {
                    ${resp.gql}
                }
            `,
            variables: resp.variables
        })
    }

}

