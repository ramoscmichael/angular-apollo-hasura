import { HttpInterceptor } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { switchMap } from "rxjs/operators";
import { GraphTokenHandler, GRAPH_TOKEN_HANDLER } from "./graph.options";

@Injectable()
export class GraphInterceptor implements HttpInterceptor {

    constructor(@Inject(GRAPH_TOKEN_HANDLER) private readonly tokenHandler: GraphTokenHandler) {}

    intercept(
        request,
        next
      ) {
        const res: any = this.tokenHandler.token()
                        .pipe(
                            switchMap(token => {
                                request = request.clone({
                                    setHeaders: token.heders
                                });
                                return next.handle(request);
                            })
                        );

        return res;
      }

}