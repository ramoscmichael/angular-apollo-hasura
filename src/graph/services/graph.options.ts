import { InjectionToken, Type } from '@angular/core';
import { Observable } from 'rxjs';

export interface GraphCoreOptions {
    WS_GRAPH_URI: string;
    HTTP_GRAPH_URI: string;
    TOKEN: Type<GraphTokenHandler>;
}

export interface GraphTokenHandler {
    token(): Observable<any>;
    subscriptionInit?();
}

export const GRAPH_TOKEN_HANDLER = new InjectionToken<GraphTokenHandler>('graph:token:handler');
export const GRAPH_CORE_OPTIONS = new InjectionToken<GraphCoreOptions>('graph:core:options');
