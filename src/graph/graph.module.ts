import { APOLLO_OPTIONS, Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { InMemoryCache, split } from '@apollo/client/core';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { GraphCoreOptions, GraphTokenHandler, GRAPH_CORE_OPTIONS, GRAPH_TOKEN_HANDLER } from './services/graph.options';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GraphService } from './services/graph.service';
import { GraphInterceptor } from './services/graph.interceptor';

function createApollo(httpLink: HttpLink, auth: GraphTokenHandler, options: GraphCoreOptions) {
  // Create an http link:
  const http = httpLink.create({ uri: options.HTTP_GRAPH_URI });

  // Create a WebSocket link:
  const ws: any = new WebSocketLink({
    uri: options.WS_GRAPH_URI,
    options: {
      reconnect: true,
      timeout: 30000,
      connectionCallback: (errors) => {
        if(errors) {
          ws.subscriptionClient.close(false, false);
        } else if(auth.subscriptionInit) {
          auth.subscriptionInit();
        }
      },
      connectionParams: () => {
        ws.connectionOptions || {};
      }
    }
  });

  const link = split(
    // split based on operation type
    ({ query }) => {
      const definition = getMainDefinition(query);
      return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    ws,
    http,
  );

  const defaultOptions = {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
    mutation: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    }
  };

  auth.token().subscribe(connectionOptions => {
    ws.connectionOptions = connectionOptions;
    // close subscription to reconnect using new token;
  });

  return {
    cache: new InMemoryCache(),
    link,
    defaultOptions
  };
}

@NgModule({
  exports: [
    HttpClientModule,
  ],
})
export class GraphCoreModule {

  static forRoot(options: GraphCoreOptions): ModuleWithProviders<any> {
    return {
      ngModule: GraphCoreModule,
      providers: [
        {
          provide: GRAPH_CORE_OPTIONS,
          useValue: options,
        },
        {
          provide: GRAPH_TOKEN_HANDLER,
          useClass: options.TOKEN,
        },
        { 
          provide: HTTP_INTERCEPTORS,
          useClass: GraphInterceptor,
          multi: true
        },
        {
          provide: APOLLO_OPTIONS,
          useFactory: createApollo,
          deps: [HttpLink, GRAPH_TOKEN_HANDLER, GRAPH_CORE_OPTIONS]
        },
        GraphService
      ]
    };
  }
}
